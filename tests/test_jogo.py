import pytest
from lib.jogo import Jogo, Jogos


@pytest.fixture
def json_1_jogo_invalido() -> dict[str, list[dict[str, str | int]] | str]:
    return {
        "input": [
            {
                "n_rodada": 1,
                "mandante": "Cruzeiro",
                "visitante": "CSA",
                "gols_mandante": -1,
                "gols_visitante": -1,
            }
        ],
        "output": "",
    }


@pytest.fixture
def json_1_jogo() -> dict[str, list[dict[str, str | int]] | str]:
    return {
        "input": [
            {
                "n_rodada": 1,
                "mandante": "Cruzeiro",
                "visitante": "CSA",
                "gols_mandante": 3,
                "gols_visitante": 1,
            }
        ],
        "output": (
            "{{LinhaTabelaJogo|estilo=|mandante=Cruzeiro" "|placar=3-1|visitante=CSA}}"
        ),
    }


@pytest.fixture
def json_1_jogo_high() -> dict[str, list[dict[str, str | int]] | str]:
    return {
        "input": [
            {
                "n_rodada": 1,
                "mandante": "Cruzeiro",
                "visitante": "CSA",
                "gols_mandante": 3,
                "gols_visitante": 1,
            }
        ],
        "output": (
            "{{LinhaTabelaJogo"
            '|estilo=style="background-color:lightblue;font-weight:bold"'
            "|mandante=Cruzeiro"
            "|placar=3-1|visitante=CSA}}"
        ),
    }


@pytest.fixture
def json_2_jogos() -> dict[str, list[dict[str, str | int]] | str]:
    return {
        "input": [
            {
                "n_rodada": 1,
                "mandante": "Cruzeiro",
                "visitante": "CSA",
                "gols_mandante": 3,
                "gols_visitante": 1,
            },
            {
                "n_rodada": 1,
                "mandante": "CRB",
                "visitante": "Vitória",
                "gols_mandante": 2,
                "gols_visitante": 0,
            },
        ],
        "output": (
            "{{LinhaTabelaJogo"
            "|estilo="
            "|mandante=Cruzeiro"
            "|placar=3-1|visitante=CSA}}\n"
            "{{LinhaTabelaJogo"
            "|estilo="
            "|mandante=CRB"
            "|placar=2-0|visitante=Vitória}}"
        ),
    }


@pytest.fixture
def json_2_jogos_high():
    return {
        "input": [
            {
                "n_rodada": 1,
                "mandante": "Cruzeiro",
                "visitante": "CSA",
                "gols_mandante": 3,
                "gols_visitante": 1,
            },
            {
                "n_rodada": 1,
                "mandante": "CRB",
                "visitante": "Vitória",
                "gols_mandante": 2,
                "gols_visitante": 0,
            },
        ],
        "output": (
            "{{LinhaTabelaJogo"
            '|estilo=style="background-color:lightblue;font-weight:bold"'
            "|mandante=Cruzeiro"
            "|placar=3-1|visitante=CSA}}\n"
            "{{LinhaTabelaJogo"
            "|estilo="
            "|mandante=CRB"
            "|placar=2-0|visitante=Vitória}}"
        ),
    }


@pytest.fixture
def json_3_jogos():
    return {
        "input": [
            {
                "n_rodada": 1,
                "mandante": "Cruzeiro",
                "visitante": "CSA",
                "gols_mandante": 3,
                "gols_visitante": 1,
            },
            {
                "n_rodada": 1,
                "mandante": "CRB",
                "visitante": "Vitória",
                "gols_mandante": 2,
                "gols_visitante": 0,
            },
            {
                "n_rodada": 2,
                "mandante": "Cruzeiro",
                "visitante": "Vitória",
                "gols_mandante": 2,
                "gols_visitante": 0,
            },
        ],
        "output": {
            2: (
                "{{LinhaTabelaJogo"
                '|estilo=style="background-color:lightblue;font-weight:bold"'
                "|mandante=Cruzeiro"
                "|placar=2-0|visitante=Vitória}}"
            ),
            1: (
                "{{LinhaTabelaJogo"
                '|estilo=style="background-color:lightblue;font-weight:bold"'
                "|mandante=Cruzeiro"
                "|placar=3-1|visitante=CSA}}\n{{LinhaTabelaJogo"
                "|estilo="
                "|mandante=CRB"
                "|placar=2-0|visitante=Vitória}}"
            ),
        },
    }


def test_load_jogo(json_1_jogo) -> None:
    jogo = Jogo(json_1_jogo["input"][0])
    assert jogo.n_rodada == 1
    assert jogo.mandante == "Cruzeiro"
    assert jogo.visitante == "CSA"
    assert jogo.gols_mandante == 3
    assert jogo.gols_visitante == 1


def test_load_jogos(json_1_jogo) -> None:
    jogo = Jogos(json_1_jogo["input"])
    assert jogo.lista[1][0].n_rodada == 1
    assert jogo.lista[1][0].mandante == "Cruzeiro"
    assert jogo.lista[1][0].visitante == "CSA"
    assert jogo.lista[1][0].gols_mandante == 3
    assert jogo.lista[1][0].gols_visitante == 1


def test_print_jogo(json_1_jogo) -> None:
    jogo: Jogo = Jogo(json_1_jogo["input"][0])
    assert jogo.mostrar() == json_1_jogo["output"]


def test_print_jogo_invalido(json_1_jogo_invalido) -> None:
    jogo = Jogo(json_1_jogo_invalido["input"][0])
    assert jogo.mostrar() == json_1_jogo_invalido["output"]


def test_print_jogo_sem_hilight(json_1_jogo) -> None:
    jogos: Jogos = Jogos(json_1_jogo["input"])
    assert jogos.mostrar(1) == json_1_jogo["output"]


def test_print_jogo_com_hilight(json_1_jogo_high) -> None:
    jogos: Jogos = Jogos(json_1_jogo_high["input"])
    assert jogos.mostrar(1, "Cruzeiro") == json_1_jogo_high["output"]


def test_print_2_jogos_sem_hilight(json_2_jogos) -> None:
    jogos = Jogos(json_2_jogos["input"])
    assert jogos.mostrar(1) == json_2_jogos["output"]


def test_print_2_jogos_com_hilight(json_2_jogos_high) -> None:
    jogos = Jogos(json_2_jogos_high["input"])
    assert jogos.mostrar(1, "Cruzeiro") == json_2_jogos_high["output"]


def test_print_3_jogos_com_hilight(json_3_jogos) -> None:
    jogos = Jogos(json_3_jogos["input"])
    assert jogos.mostrar(1, "Cruzeiro") == json_3_jogos["output"][1]
    assert jogos.mostrar(2, "Cruzeiro") == json_3_jogos["output"][2]

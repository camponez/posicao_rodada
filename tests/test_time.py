"""Tests time."""

from typing import Any, Dict, List

import pytest
from lib.output import linha_estruturada
from lib.time import Time, Times
from mock import MagicMock


def test_criar_time() -> None:
    tim: Time = Time(1, "Cruzeiro", "CRU")

    assert tim.id_time == 1
    assert tim.nome == "Cruzeiro"
    assert tim.nome_abrv == "CRU"


def test_lista_times() -> None:
    con: MagicMock = MagicMock()
    row: MagicMock = MagicMock()

    con.execute.return_value = [
        [MagicMock(id_time=1, nome="Cruzeiro", nome_abrv="CRU")],
        [MagicMock(id_time=2, nome="Avaí", nome_abrv="AVA")],
    ]

    tims: Times = Times(con)

    assert len(tims.l_times) == 2
    assert tims.return_id("Cruzeiro") == 1
    assert tims.return_name(2) == ("AVA", "Avaí")


def test_invalid_time_id() -> None:
    con: MagicMock = MagicMock()
    con.cursor().execute.return_value = [[1, "Cruzeiro", "CRU"], [2, "Avaí", "AVA"]]

    tims: Times = Times(con)

    with pytest.raises(Exception):
        tims.return_id("Czeiro")


def test_invalid_time_name() -> None:
    con: MagicMock = MagicMock()
    con.cursor().execute.return_value = [[1, "Cruzeiro", "CRU"], [2, "Avaí", "AVA"]]

    tims: Times = Times(con)

    assert tims.return_name(3) == ()


def test_linha_estruturada() -> None:
    linha: MagicMock = MagicMock(
        pontos=10, vitorias=2, golsp=5, saldo=2, jogos=8, empate=2
    )

    result: Dict[str, int] = {
        "pontos": 10,
        "jogos": 8,
        "vitorias": 2,
        "empate": 2,
        "derrotas": 4,
        "golspro": 5,
        "golscontra": 3,
        "saldo": 2,
    }

    assert linha_estruturada(linha) == result

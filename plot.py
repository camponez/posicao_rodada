import json
import csv
import argparse
import matplotlib.pyplot as plt


def main(arquivo_rodada) -> None:
    jogos: Any = json.loads(open(arquivo_rodada, "r", encoding="utf-8").read())

    def get_pontos_team(team, pts, n_rodadas):
        pontos = [pts]

        time_disputa = False
        for i in jogos["rodadas"]:
            if i["mandante"] != team and i["visitante"] != team:
                continue

            if i["n_rodada"] >= n_rodadas:
                continue

            time_disputa = True
            if i["gols_mandante"] == i["gols_visitante"]:
                pontos.append(pontos[-1] + 1)

            if (
                i["mandante"] == team
                and i["gols_mandante"] > i["gols_visitante"]
            ):
                pontos.append(pontos[-1] + 3)

            if (
                i["visitante"] == team
                and i["gols_mandante"] < i["gols_visitante"]
            ):
                pontos.append(pontos[-1] + 3)

            if (
                i["mandante"] == team
                and i["gols_mandante"] < i["gols_visitante"]
            ):
                pontos.append(pontos[-1])

            if (
                i["visitante"] == team
                and i["gols_mandante"] > i["gols_visitante"]
            ):
                pontos.append(pontos[-1])

        if time_disputa:
            return pontos

        return time_disputa

    N_RODADAS = 38

    PONTOS = range(N_RODADAS)

    # print(len(get_pontos_team('Avaí', 0, N_RODADAS)))

    TIMES = []

    with open("times.csv", "r", encoding="utf-8") as f_time:
        csvfile = csv.DictReader(f_time)
        for i in csvfile:
            TIMES.append([i["nome"], i["nome_abrv"]])

    for t in TIMES:
        pontos_time = get_pontos_team(t[0], 0, N_RODADAS)
        if pontos_time:
            plt.plot(PONTOS, pontos_time, ".", label=t[1], dashes=[6, 2])

    plt.xlim(1, 40)
    plt.ylim(0, 90)
    plt.legend()
    plt.show()


if __name__ == "__main__":
    PARSER = argparse.ArgumentParser(description="Plot")
    PARSER.add_argument(
        "-f", "--file", help="arquivo para processar", required=True
    )

    ARGS = PARSER.parse_args()
    main(ARGS.file)

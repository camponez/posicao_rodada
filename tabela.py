"""Module tabela."""

import argparse
import csv
import json
import sys
import warnings
from typing import Dict, Union

from sqlalchemy import Engine, and_, create_engine, func, insert, or_, select
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import Session

from lib.output import NotWiki, Wiki
from tabelas_db import BaseTable, Rodada, Tabela, TabelaTemp, TimesTBL


def commit(session):
    try:
        session.commit()
    except IntegrityError:
        print(session.query())
        sys.exit(1)
    except BaseException:
        raise
        sys.exit(1)


def load_rodadas(session, f_rodadas, ate_rodada=38) -> None:
    """Load rodadas."""
    with open(f_rodadas, "rb") as fp_rodada:
        jogos: dict = json.load(fp_rodada)

    for rodada in jogos["rodadas"]:
        if rodada["n_rodada"] > ate_rodada:
            continue

        if -1 in [rodada["gols_mandante"], rodada["gols_visitante"]]:
            messagem_erro: str = (
                "Placar inválido: "
                f"{rodada['n_rodada']} - {rodada['mandante']}"
                f" - {rodada['visitante']}"
            )
            warnings.warn(messagem_erro)
            continue

        session.add(Rodada(**rodada))

        commit(session)


def load_times(session: Session) -> None:
    """Load times."""
    with open("times.csv", "r") as file:
        csv_reader = csv.reader(file)
        header = next(csv_reader)

        for row in csv_reader:
            data = dict(zip(header, row))
            new_entry = TimesTBL(**data)
            session.add(new_entry)

        commit(session)


def load_tabela(session: Session, f_rodada: int, n_rodada: int) -> None:
    """Load tabela."""
    times = session.execute(select(TimesTBL))

    for i in times:
        time_nome: str = i[0].nome
        jogos = len(
            session.execute(
                select(Rodada).where(
                    or_(Rodada.mandante == time_nome, Rodada.visitante == time_nome)
                )
            ).all()
        )
        if not jogos:
            continue

        vitoria_mandante = and_(
            Rodada.mandante == time_nome,
            Rodada.gols_mandante > Rodada.gols_visitante,
        )
        vitoria_visitante = and_(
            Rodada.visitante == time_nome,
            Rodada.gols_mandante < Rodada.gols_visitante,
        )
        vitorias_stm = select(Rodada).where(or_(vitoria_mandante, vitoria_visitante))

        empates_stm = select(Rodada).where(
            and_(Rodada.gols_mandante == Rodada.gols_visitante),
            or_(Rodada.mandante == time_nome, Rodada.visitante == time_nome),
        )

        def stm_count(stm):
            return len(session.execute(stm).all())

        vitorias: int = stm_count(vitorias_stm)
        empates: int = stm_count(empates_stm)

        pontos: int = (vitorias * 3) + empates
        with open(f_rodada, "rb") as f_file:
            l_rodadas: Dict = json.loads(f_file.read())
            if "ajustes" in l_rodadas:
                ajuste: Dict = l_rodadas["ajustes"]
                nome_time: str = i[0].nome
                if nome_time in ajuste and n_rodada >= int(ajuste[nome_time]["rodada"]):
                    pontos += ajuste[nome_time]["pontos"]

        smt_gols_mandante = (
            select(func.sum(Rodada.gols_mandante))
            .select_from(Rodada)
            .where(Rodada.mandante == time_nome)
        )
        smt_gols_visitante = (
            select(func.sum(Rodada.gols_visitante))
            .select_from(Rodada)
            .where(Rodada.visitante == time_nome)
        )

        gols_pro_mandante: int | None = session.execute(smt_gols_mandante).scalar()
        gols_pro_visitante: int | None = session.execute(smt_gols_visitante).scalar()

        if not gols_pro_mandante:
            gols_pro_mandante = 0

        if not gols_pro_visitante:
            gols_pro_visitante = 0

        gols_pro: int = gols_pro_mandante + gols_pro_visitante

        smt_gols_contra_mandante = (
            select(func.sum(Rodada.gols_visitante))
            .select_from(Rodada)
            .where(Rodada.mandante == time_nome)
        )
        smt_gols_contra_visitante = (
            select(func.sum(Rodada.gols_mandante))
            .select_from(Rodada)
            .where(Rodada.visitante == time_nome)
        )

        gols_contra_mandante: int | None = session.execute(
            smt_gols_contra_mandante
        ).scalar()
        gols_contra_visitante: int | None = session.execute(
            smt_gols_contra_visitante
        ).scalar()

        if not gols_contra_mandante:
            gols_contra_mandante = 0

        if not gols_contra_visitante:
            gols_contra_visitante = 0

        gols_contra: int = gols_contra_mandante + gols_contra_visitante

        saldo: int = gols_pro - gols_contra

        session.execute(
            insert(Tabela),
            [
                {
                    "id_time": i[0].id_time,
                    "pontos": pontos,
                    "vitorias": vitorias,
                    "saldo": saldo,
                    "golsp": gols_pro,
                    "jogos": jogos,
                    "empate": empates,
                }
            ],
        )

    times = session.execute(select(TimesTBL))
    for i in times:
        time_nome = i[0].nome

        jogos = len(
            session.execute(
                select(Rodada).where(
                    and_(
                        Rodada.n_rodada < n_rodada,
                        or_(
                            Rodada.mandante == time_nome, Rodada.visitante == time_nome
                        ),
                    )
                )
            ).all()
        )
        if not jogos:
            continue

        vitoria_mandante = and_(
            Rodada.mandante == time_nome,
            Rodada.gols_mandante > Rodada.gols_visitante,
            Rodada.n_rodada < n_rodada,
        )
        vitoria_visitante = and_(
            Rodada.visitante == time_nome,
            Rodada.gols_mandante < Rodada.gols_visitante,
            Rodada.n_rodada < n_rodada,
        )
        vitorias_stm = select(Rodada).where(or_(vitoria_mandante, vitoria_visitante))

        empates_stm = select(Rodada).where(
            Rodada.gols_mandante == Rodada.gols_visitante,
            Rodada.n_rodada < n_rodada,
            or_(Rodada.mandante == time_nome, Rodada.visitante == time_nome),
        )

        def stm_count(stm):
            return len(session.execute(stm).all())

        vitorias: int = stm_count(vitorias_stm)
        empates: int = stm_count(empates_stm)

        pontos: int = (vitorias * 3) + empates
        with open(f_rodada, "rb") as f_file:
            l_rodadas: Dict = json.loads(f_file.read())
            if "ajustes" in l_rodadas:
                ajuste: Dict = l_rodadas["ajustes"]
                nome_time: str = i[0].nome
                if nome_time in ajuste and n_rodada >= int(ajuste[nome_time]["rodada"]):
                    pontos += ajuste[nome_time]["pontos"]

        smt_gols_mandante = (
            select(func.sum(Rodada.gols_mandante))
            .select_from(Rodada)
            .where(Rodada.mandante == time_nome, Rodada.n_rodada < n_rodada)
        )
        smt_gols_visitante = (
            select(func.sum(Rodada.gols_visitante))
            .select_from(Rodada)
            .where(Rodada.visitante == time_nome, Rodada.n_rodada < n_rodada)
        )

        gols_pro_mandante: int = (
            0
            if not session.execute(smt_gols_mandante).scalar()
            else session.execute(smt_gols_mandante).scalar()
        )
        gols_pro_visitante: int = (
            0
            if not session.execute(smt_gols_visitante).scalar()
            else session.execute(smt_gols_visitante).scalar()
        )

        gols_pro: int = gols_pro_mandante + gols_pro_visitante

        smt_gols_contra_mandante = (
            select(func.sum(Rodada.gols_visitante))
            .select_from(Rodada)
            .where(Rodada.mandante == time_nome, Rodada.n_rodada < n_rodada)
        )
        smt_gols_contra_visitante = (
            select(func.sum(Rodada.gols_mandante))
            .select_from(Rodada)
            .where(Rodada.visitante == time_nome, Rodada.n_rodada < n_rodada)
        )

        gols_contra_mandante = session.execute(smt_gols_contra_mandante).scalar()
        gols_contra_visitante = session.execute(smt_gols_contra_visitante).scalar()

        gols_contra = (gols_contra_mandante or 0) + (gols_contra_visitante or 0)

        saldo = gols_pro - gols_contra

        session.execute(
            insert(TabelaTemp),
            [
                {
                    "id_time": i[0].id_time,
                    "pontos": pontos,
                    "vitorias": vitorias,
                    "saldo": saldo,
                    "golsp": gols_pro,
                    "jogos": jogos,
                    "empate": empates,
                }
            ],
        )

    session.commit()


def main(session, f_rodadas, n_rodada, n_time, cor_fundo, output) -> None:
    """Run main method."""
    load_times(session)
    load_rodadas(session, f_rodadas, n_rodada)
    load_tabela(session, f_rodadas, n_rodada)

    wiki: Union[Wiki, NotWiki] = Wiki(session, f_rodadas, n_rodada, n_time, cor_fundo)

    if output == "nowiki":
        wiki = NotWiki(session, n_time)

    wiki.mostrar_tabela()


if __name__ == "__main__":
    PARSER = argparse.ArgumentParser(description="Tabela de jogos")
    PARSER.add_argument("-f", "--file", help="arquivo para processar", required=True)
    PARSER.add_argument(
        "-o",
        "--output",
        help="formato: wiki (default), nowiki",
        default="wiki",
    )
    PARSER.add_argument(
        "-r",
        "--rodada",
        help="rodada para analisar",
        default=38,
        required=True,
        type=int,
    )
    PARSER.add_argument(
        "-t",
        "--time",
        help="time para destacar. default: Cruzeiro",
        default="Cruzeiro",
    )
    PARSER.add_argument(
        "-c",
        "--cor_fundo",
        help="time para destacar. default: lightblue",
        default="lightblue",
    )
    ARGS = PARSER.parse_args()

    engine: Engine = create_engine("sqlite+pysqlite:///:memory:", echo=False)
    BaseTable.metadata.create_all(engine)
    session: Session = Session(engine)

    # load_times(session)
    # load_rodadas(session, ARGS.file, ARGS.rodada)
    # load_tabela_v2(session, ARGS.file, ARGS.rodada)

    main(
        session, ARGS.file, ARGS.rodada, ARGS.time, ARGS.cor_fundo, ARGS.output.lower()
    )

# vim: tw=100 colorcolumn=100

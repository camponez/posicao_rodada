from typing import List, Tuple

from sqlalchemy import select
from tabelas_db import TimesTBL


class Time:
    """Class Time."""

    def __init__(self, id_time, nome, nome_abrv) -> None:
        """Init."""

        self._id_time = id_time
        self._nome = nome
        self._nome_abrv = nome_abrv

    @property
    def id_time(self) -> int:
        """ID Time."""

        return self._id_time

    @property
    def nome(self) -> str:
        """Nome."""

        return self._nome

    @property
    def nome_abrv(self):
        """Nome abreviation."""

        return self._nome_abrv


class Times:
    """Times."""

    def __init__(self, conn) -> None:
        """Init."""

        self.conn = conn
        self.l_times = []  # type: List[Time]
        self.load_times()

    def load_times(self) -> None:
        """Load times."""
        rows = self.conn.execute(select(TimesTBL).order_by(TimesTBL.nome))

        for i in rows:
            self.l_times.append(Time(i[0].id_time, i[0].nome, i[0].nome_abrv))

    def return_id(self, nome: str) -> str:
        """return id."""
        for i in self.l_times:
            if nome == i.nome:
                return i.id_time

        raise Exception(f"Time não encontrado: {nome}")

    def return_name(self, id_time: int) -> Tuple:
        """return nome."""
        for i in self.l_times:
            if id_time == i.id_time:
                return (i.nome_abrv, i.nome)

        return ()

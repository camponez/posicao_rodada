"""Modulo jogo."""

from typing import Dict


class Jogo:
    def __init__(self, jogo) -> None:
        self.n_rodada: int = jogo["n_rodada"]
        self.mandante: str = jogo["mandante"]
        self.visitante: str = jogo["visitante"]
        self.gols_mandante: int = jogo["gols_mandante"]
        self.gols_visitante: int = jogo["gols_visitante"]
        self.estilo: str = 'style="background-color:lightblue;font-weight:bold"'

    def mostrar(self, time_high: str = "") -> str:
        """Mostra a linha do jogo."""
        if -1 in (self.gols_visitante, self.gols_mandante):
            return ""

        if time_high not in [self.mandante, self.visitante]:
            self.estilo = ""
        return (
            "{{LinhaTabelaJogo"
            f"|estilo={self.estilo}"
            f"|mandante={self.mandante}"
            f"|placar={self.gols_mandante}-{self.gols_visitante}"
            f"|visitante={self.visitante}"
            "}}"
        )


class Jogos:
    def __init__(self, jogos) -> None:
        self.lista: Dict[int, list] = {}
        for jogo in jogos:
            if jogo["n_rodada"] not in self.lista.keys():
                self.lista[jogo["n_rodada"]] = []
            self.lista[jogo["n_rodada"]].append(Jogo(jogo))

    def mostrar(self, n_rodada, time_high: str = "") -> str:
        output: str = ""
        for jogo in self.lista[n_rodada]:
            output += jogo.mostrar(time_high) + "\n"
        return output[:-1]

"""Output."""
import json
from abc import ABC, abstractmethod
from typing import Any, Dict, List, Tuple

from sqlalchemy import desc, select
from tabelas_db import Rodada, Tabela, TabelaTemp

from lib.jogo import Jogos
from lib.time import Times


def linha_estruturada(linha) -> Dict[str, int]:
    """Estrutura da linha."""
    formated_line: Dict[str, int] = {}
    formated_line["pontos"] = linha.pontos
    formated_line["jogos"] = linha.jogos
    formated_line["vitorias"] = linha.vitorias
    formated_line["empate"] = linha.empate
    formated_line["derrotas"] = linha.jogos - linha.vitorias - linha.empate
    formated_line["golspro"] = linha.golsp
    formated_line["golscontra"] = linha.golsp - linha.saldo
    formated_line["saldo"] = linha.saldo

    return formated_line


class Output(ABC):
    """Class Output."""

    def __init__(self, session, time_atual, cor_fundo) -> None:
        # sql: str = (
        #     "select id_time, pontos, vitorias, golsp, saldo, jogos, empate "
        #     " FROM tabela ORDER BY "
        #     "pontos desc, vitorias desc, saldo desc, golsp desc"
        # )
        self.tabela: Any = session.execute(
            select(Tabela).order_by(
                desc(Tabela.pontos),
                desc(Tabela.vitorias),
                desc(Tabela.saldo),
                desc(Tabela.golsp),
            )
        )

        self.conn: Any = session
        self.times = Times(self.conn)

        self.line_style: str = ""
        self.linha: List[int] = []
        self.pos: str = ""
        self.end: str = ""
        self.index: int = 1
        self.time_atual: str = time_atual
        self.cor_fundo: str = cor_fundo

    @staticmethod
    def cabecalho() -> None:
        """Cabecalho."""

    @staticmethod
    def rodape() -> None:
        """Rodapé."""

    def mostrar_tabela(self) -> None:
        """Mostrar tabela."""

    @abstractmethod
    def mostrar_jogos(self) -> None:
        """Mostrar jogos."""


class Wiki(Output):
    """Wiki format."""

    def __init__(self, conn, f_rodadas, n_rodada, n_time, cor_fundo) -> None:
        """__init__."""
        super().__init__(conn, n_time, cor_fundo)
        self.name: str = "Wiki"
        self.rodadas: int = f_rodadas
        self.n_rodada: int = n_rodada
        self.tabela_temp: Any = None

    @staticmethod
    def cabecalho() -> None:
        print('{|class="wikitable"')
        print("!Pos!!Time!! - !!J!!P!!V!!E!!D!!GP!!GC!!SG!!Apr.")

    def mostrar_tabela(self) -> None:
        """Mostrar tabela"""

        print("{|")
        print("|")
        self.cabecalho()
        for self.linha in self.tabela:
            self.linha = self.linha[0]
            self.pos = "--"
            self.end = ""
            self.line_style = ""
            if self.index <= 4:
                self.pos = "G4"
                self.line_style = ' style="background-color:#B1F2B4"'
            if self.index >= 17:
                self.pos = "Z4"
                self.line_style = ' style="background-color:#F7CBCB'

            if self.times.return_name(self.linha.id_time)[1] == self.time_atual:
                self.pos = "=>"
                self.end = "<="
                self.line_style = (
                    f' style="background-color:{self.cor_fundo};font-weight:bold"'
                )

            self.conteudo()
            self.index += 1
        self.rodape()
        self.mostrar_jogos()

    def achar_posicao(self, time: Tuple) -> int:
        self.tabela_temp = self.conn.execute(
            select(TabelaTemp).order_by(
                desc(TabelaTemp.pontos),
                desc(TabelaTemp.vitorias),
                desc(TabelaTemp.saldo),
                desc(TabelaTemp.golsp),
            )
        ).all()

        index: int = 1
        for i in self.tabela_temp:
            if time == self.times.return_name(i[0].id_time):
                return index
            index += 1

        return index

    def conteudo(self) -> None:
        """conteudo."""
        # posicao nome
        linha: str = "{{LinhaTabela" f"|estilo={self.line_style}|posicao={self.index}º"

        linha_tabela: Dict[str, int] = linha_estruturada(self.linha)
        sinal: str = ""
        if linha_tabela["jogos"] > 1:
            index_anterior: int = self.achar_posicao(
                self.times.return_name(self.linha.id_time)
            )
            if index_anterior > self.index:
                sinal = "<small>({} [[Arquivo:UpGreen.png|10px]])</small>"
            elif index_anterior < self.index:
                sinal = "<small>({} [[Arquivo:DownRed.png|10px]])</small>"
            else:
                sinal = "<small>({} [[Arquivo:SamePos.png|10px]])</small>"

            sinal = sinal.format(abs(index_anterior - self.index))

        linha = "{{LinhaTabela" f"|estilo={self.line_style}|posicao={self.index}º"

        linha += (
            f"|clube={self.times.return_name(self.linha.id_time)[1]}"
            f"|clube_abr={self.times.return_name(self.linha.id_time)[0]}"
            f"|sinal={sinal}"
        )

        for i in [
            "pontos",
            "jogos",
            "vitorias",
            "empate",
            "derrotas",
            "golspro",
            "golscontra",
            "saldo",
        ]:
            linha += f"|{i}={linha_tabela[i]}"

        if self.linha.jogos == 0:
            aprov = 0.00
        else:
            aprov = ((self.linha.vitorias * 3) + self.linha.empate) / (
                int(self.linha.jogos) * 3
            )
        linha += f"|aprov={aprov*100:.2f}%"
        linha += "}}"

        print(linha)

    @staticmethod
    def rodape() -> None:
        """Rodape."""
        print("|}")

    def mostrar_jogos(self) -> None:
        """Mostrar jogos."""
        print("|")
        print('{|class="wikitable"')
        with open(self.rodadas, "rb") as r_rodadas:
            json_jogos = json.load(r_rodadas)

        jogos: Jogos = Jogos(json_jogos["rodadas"])
        print(jogos.mostrar(self.n_rodada, self.time_atual))

        self.rodape()
        print("|}")


class NotWiki(Output):
    """Output not wiki"""

    def __init__(self, conn, n_time: str) -> None:
        super().__init__(conn, "", "")
        self.name = "NotWiki"
        self.n_time = n_time

    def conteudo(self) -> None:
        n_time_abr = self.times.return_name(self.linha[0])[0]
        n_time = self.times.return_name(self.linha[0])[1]
        output = (
            f"({self.pos}) {self.index:2d}: {n_time_abr} "
            f"- {int(self.linha[1]):2d}P - {int(self.linha[2]):2d}V "
            f"- {int(self.linha[3]):2d}GP - {self.linha[4]:3d}S "
            f"- {self.linha[5]:2d}J {self.end}"
        )

        if n_time == self.n_time:
            output = f"==>{output}<=="

        print(output)

    def mostrar_jogos(self):
        """Mostrar jogos"""

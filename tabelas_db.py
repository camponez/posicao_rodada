from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column


class BaseTable(DeclarativeBase):
    pass


class TimesTBL(BaseTable):
    """Nomes dos times."""

    __tablename__: str = "times"
    id_time: Mapped[int] = mapped_column(primary_key=True)
    nome: Column = Column(String)
    nome_abrv: Column = Column(String)

    # def __repr__(self) -> str:
    #     return f"{self.id_time} - {self.nome} - {self.nome_abrv}"


class Rodada(BaseTable):
    """Tabela Rodada."""

    __tablename__: str = "rodada"
    n_rodada: Mapped[int] = mapped_column(
        ForeignKey(TimesTBL.id_time), primary_key=True
    )
    mandante = mapped_column(ForeignKey(TimesTBL.id_time), primary_key=True)
    visitante: Mapped[int] = mapped_column(
        ForeignKey(TimesTBL.id_time), primary_key=True
    )
    gols_mandante: Column = Column(Integer)
    gols_visitante: Column = Column(Integer)

    # def __repr__(self) -> str:
    #     return (
    #         f"{self.n_rodada} - {self.mandante} "
    #         f"{self.gols_mandante}x{self.gols_visitante} {self.visitante}"
    #     )


class Tabela(BaseTable):
    """Tabela."""

    __tablename__: str = "tabela"
    pk_id: Mapped[int] = mapped_column(primary_key=True)
    jogos: Column = Column(Integer)
    id_time: Mapped[int] = mapped_column(ForeignKey("times.id_time"))
    pontos: Column = Column(Integer)
    vitorias: Column = Column(Integer)
    empate: Column = Column(Integer)
    saldo: Column = Column(Integer)
    golsp: Column = Column(Integer)


class TabelaTemp(BaseTable):
    """Tabela."""

    __tablename__: str = "tabela_temp"
    pk_id: Mapped[int] = mapped_column(primary_key=True)
    jogos: Column = Column(Integer)
    id_time: Column = Column(Integer)
    pontos: Column = Column(Integer)
    vitorias: Column = Column(Integer)
    saldo: Column = Column(Integer)
    golsp: Column = Column(Integer)
